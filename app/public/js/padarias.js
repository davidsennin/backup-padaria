angular.module('padarias', ['ngRoute', 'pdrdirectives', 'pdrmenumod', 'pdrTable', 'pdrPanels', 'ngResource',
'ngAnimate', 'fmDiversos', 'pdrDataServices', 'pdrDataStore', 'fmMasks', 'fmSearchPanel', 'pdrRelatorios'])
.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/produtos', {
    templateUrl: '/partials/produtos.html',
    controller: 'ProdutosController'
  });

  $routeProvider.when('/estoque', {
    templateUrl: '/partials/lancamento-estoque.html',
    controller: 'LancamentosEstoqueController'
  });

  $routeProvider.when('/itens-estoque', {
    templateUrl: '/partials/itens-estoque.html',
    controller: 'ItensEstoqueController'
  });

  $routeProvider.when('/vendas', {
    templateUrl: '/partials/lancamentos-vendas.html',
    controller: 'VendasController'
  });

  $routeProvider.when('/vendas/relatorio/:tipoRelatorio', {
    templateUrl: '/partials/relatorio.html',
    controller: 'RelatorioController'
  });

  $routeProvider.when('/desperdicio', {
    templateUrl: '/partials/lancamentos-desperdicios.html',
    controller: 'DesperdiciosController'
  });

  $routeProvider.when('/desperdicio/relatorio/:tipoRelatorio', {
    templateUrl: '/partials/relatorio.html',
    controller: 'RelatorioController'
  });

  $routeProvider.otherwise({
    redirectTo: '/produtos'
  });

}])
.run(['pdrServerProdutos', 'pdrServerLancamentosEstoque', 'pdrServerLancamentosVenda', 'pdrServerLancamentosDesperdicio',
'ProdutosDataService', 'EstoqueDataService', 'VendasDataService', 'DesperdiciosDataService',
function(pdrServerProdutos, pdrServerLancamentosEstoque, pdrServerLancamentosVenda, pdrServerLancamentosDesperdicio,
  ProdutosDataService, EstoqueDataService, VendasDataService, DesperdiciosDataService) {

  function loadError(error) {
    console.log(error);
    throw new Error("Ocorreu um erro na comunicação com o servidor ao carregar os dados");
  }
  pdrServerProdutos.listar()
    .then(function(produtos) {
      ProdutosDataService.populate(produtos);
    })
    .catch(loadError);
  pdrServerLancamentosEstoque.listar()
    .then(function(lancamentos) {
      EstoqueDataService.populate(lancamentos);
    })
    .catch(loadError);
  pdrServerLancamentosVenda.listar()
    .then(function(lancamentos) {
      VendasDataService.populate(lancamentos);
    })
    .catch(loadError);
  pdrServerLancamentosDesperdicio.listar()
    .then(function(lancamentos) {
      DesperdiciosDataService.populate(lancamentos);
    })
    .catch(loadError);
}]);

(function() {

  let pdrItensEstoque = angular.module('pdrDataStore');

  pdrItensEstoque.factory('ItensEstoqueDataService', ['EstoqueDataService', 'makeInherit', 'ProdutosDataService', 'VendasDataService', 'DesperdiciosDataService',
  function(EstoqueDataService, makeInherit, ProdutosDataService, VendasDataService, DesperdiciosDataService) {
    const LANCAMENTO_ITEM = 1;
    const VENDA_ITEM = 2;
    const DESPERDICIO = 3;
    let self;

    function ItensEstoqueDataService() {
    }

    function _calculateQtd(qtd, type) {
      if (type == LANCAMENTO_ITEM) {
        return qtd;
      }
      return 0-qtd;
    }

    function _calculateItem(item, reduced, type) {
      let prod = ProdutosDataService.getProdutoById(item.produto_id);
      let red = reduced[prod.nome];
      if (!red) {
        reduced[prod.nome] = {nome: prod.nome, custo: prod.custo, valor: prod.valor, quantidade: _calculateQtd(item.quantidade, type)};
      } else {
        reduced[prod.nome].quantidade += _calculateQtd(item.quantidade, type);
      }
    }

    function _reduceData(array, reduced, type) {
      array.forEach((item) => {
        _calculateItem(item, reduced, type);
      });
    }

    function _transformToArray(object, array) {
      let keys = Object.keys(object);
      keys.forEach((key) => {
        array.push(object[key]);
      });
    }

    ItensEstoqueDataService.prototype.getItens = function() {
      let lancamentoEstoque = EstoqueDataService.getEstoqueFull();
      let lancamentoVendas = VendasDataService.getVendasFull();
      let lancamentoDesperdicios = DesperdiciosDataService.getDesperdiciosFull();
      let reduced = {};
      _reduceData(lancamentoEstoque, reduced, LANCAMENTO_ITEM);
      _reduceData(lancamentoVendas, reduced, VENDA_ITEM);
      _reduceData(lancamentoDesperdicios, reduced, DESPERDICIO);
      let reducedArray = [];
      _transformToArray(reduced, reducedArray);
      console.log(reducedArray);
      return reducedArray;
    }

    self = new ItensEstoqueDataService();
    return self;

  }]);

})();

(function() {

  let pdrDesperdicios = angular.module('pdrDataStore');

  pdrDesperdicios.factory('DesperdiciosDataService', ['PdrLancamentos', 'ProdutosDataService', 'makeInherit', 'presentableDate',
  'pdrServerLancamentosDesperdicio',
  function(PdrLancamentos, ProdutosDataService, makeInherit, presentableDate, pdrServerLancamentosDesperdicio) {

    function DesperdiciosDataService() {
      PdrLancamentos.call(this);
    }
    makeInherit(DesperdiciosDataService, PdrLancamentos);

    DesperdiciosDataService.prototype.getServer = function() {
      return pdrServerLancamentosDesperdicio;
    }
    DesperdiciosDataService.prototype.deleteWaste = function(waste) {
      waste = this.cleanParam(waste);
      waste = this.getDataById(waste);
      this.delete(waste).setTipoTexto("Desperdicios");
    }
    DesperdiciosDataService.prototype.criarDesperdicio = function(desperdicio, callback) {
      let newData = this.createData(desperdicio);
      this.addData(newData, callback);
    }
    DesperdiciosDataService.prototype.getDesperdicios = function() {
      return this.getData();
    }
    DesperdiciosDataService.prototype.getDesperdiciosFull = function() {
      return this.getFullData();
    }
    return new DesperdiciosDataService();



  }]);

})();

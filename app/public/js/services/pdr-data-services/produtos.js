(function() {

  let pdrProdutos = angular.module('pdrDataStore');

  pdrProdutos.factory('ProdutosDataService', ['PdrDataService', 'pdrConfimDeletePanel', 'makeInherit', 'pdrServerProdutos',
  function(PdrDataService, pdrConfimDeletePanel, makeInherit, pdrServerProdutos) {

    function ProdutosDataService() {
      PdrDataService.call(this);
    }
    makeInherit(ProdutosDataService, PdrDataService);

    ProdutosDataService.prototype.deleteProduto = function(produto) {
      this.delete(produto).setTipoTexto("Produto");
    }
    ProdutosDataService.prototype.getServer = function() {
      return pdrServerProdutos;
    }
    ProdutosDataService.prototype.criaProduto = function(produto, callback) {
      produto.custo = parseFloat(produto.custo);
      produto.preco = parseFloat(produto.preco);
      this.addData(produto, callback);
    }
    ProdutosDataService.prototype.getProdutoById = function(id) {
      id = this.cleanParam(id);
      return this.getDataBy("_id", {_id: id});
    }
    ProdutosDataService.prototype.getProdutoByNome = function(_nome) {
      _nome  =this.cleanParam(_nome);
      return this.getDataBy("nome", {nome: _nome});
    }

    return new ProdutosDataService();

  }]);
})();

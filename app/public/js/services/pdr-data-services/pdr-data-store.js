(function() {

  let pdrDataService = angular.module('pdrDataStore', ['fmDataServices', 'fmDataSet', 'fmDiversos']);

  pdrDataService.factory('PdrDataService', ['FMDataSet', 'normalizeParam', 'Listener', 'pdrConfimDeletePanel',
  function(FMDataSet, normalizeParam, Listener, pdrConfimDeletePanel) {

    function DataService() {
      this._dataSet = new FMDataSet();
      this._editSetListener = undefined;
    }

    DataService.prototype.populate = function(lancamentos) {
      let self = this;
      lancamentos = lancamentos || [];
      lancamentos.forEach((lancamento) => {
        self._dataSet.addData(lancamento);
      });
    }

    DataService.prototype.deleteData = function(data) {
      try {
        this._dataSet.deleteData(data);
      } catch (e) {
        console.log(e);
      }
    }
    DataService.prototype.getDataById = function(data) {
      data = normalizeParam(data);
      return this.getDataBy("_id", data);
    }
    DataService.prototype.setDataToEdit = function(data) {
      data = normalizeParam(data);
      // Como estamos usando um OrderedArray, a key passada não é importante
      //Pois o array usará a key que está sendo usada para ordenação do elementos
      // Só que não mais. As classes de array serão revisadas no futuro (eu espero...)
      let d0 = this.getDataById(data);
      if (d0) {
        if (this._editSetListener) {
          d0 = angular.copy(d0);
          d0 = this._makeItemToScreen(d0);
          this._editSetListener.run(d0);
        }
      } else {
        throw new Error("A informação passada não foi encontrada na lista");
      }
    }
    DataService.prototype._makeItemToScreen = function(data) {
      return data;
    }
    DataService.prototype.getData = function() {
      self = this;
      let data = this._dataSet.getData();
      let result = [];
      data.forEach((d) => result.push(self._makeItemToTable(d)));
      return result;
    }
    DataService.prototype.getFullData = function() {
      return this._dataSet.getData();
    }
    DataService.prototype.getDataBy = function(key, value) {
      return this._dataSet.getDataBy(key, value);
    }
    DataService.prototype.delete = function(data, confirmCallback, cancelCallback) {
      self = this;
      data = self.cleanParam(data);

      function confirmDelete() {
        let server = self.getServer();
        if (server) {
          if (confirmCallback) {
            confirmCallback(data);
          }
          server.deleta(data)
            .then(function() {
              self.deleteData(data);
            })
            .catch(function(error) {
              pdrConfimDeletePanel
                .openInformPanel()
                .setMessage("Não foi possível excluir o produto")
                .setMot(error.data);
            });
        } else {
          self.deleteData(data);
        }
      }

      function cancelDelete() {
        if (cancelCallback) {
          cancelCallback();
        }
      }

      return pdrConfimDeletePanel
        .openConfirmCancelPanel(confirmDelete, cancelDelete)
        .setItem(data);
    }
    DataService.prototype._deleteData = function(dataParam) {
      this._dataSet.deleteData(dataParam);
    }
    DataService.prototype.getServer = function() {
      return undefined;
    }
    DataService.prototype.addData = function(data, callback) {
      let self = this;
      let server = this.getServer();
      if (server) {
        server.criar(data)
          .then(function(created) {
            if (created._id) {
              data._id = created._id;
            }
            self._dataSet.addData(data);
            if (callback) {
              callback();
            }
          })
          .catch(function(err) {
            if (callback) {
              callback(err);
            }
          });
      } else {
        this._dataSet.addData(data);
      }
    }
    DataService.prototype.listeners = function() {
      return this._dataSet.listeners();
    }
    DataService.prototype.putEditSetListener = function(fn) {
      this._editSetListener = new Listener(1, fn);
    }

    DataService.prototype.clearEditSetListener = function() {
      this._editListener = undefined;
    }
    DataService.prototype._makeItemToTable = function(data) {
      return data;
    }
    DataService.prototype.cleanParam = function(param) {
      return normalizeParam(param);
    }

    return DataService;

  }]);

})();

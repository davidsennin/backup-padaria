(function() {

  let pdrEstoque = angular.module('pdrDataStore');

  pdrEstoque.factory('EstoqueDataService', ['PdrLancamentos', 'makeInherit', 'ProdutosDataService', 'pdrServerLancamentosEstoque',
  function(PdrLancamentos, makeInherit, ProdutosDataService, pdrServerLancamentosEstoque) {
    function EstoqueDataService() {
      PdrLancamentos.call(this);
    }
    makeInherit(EstoqueDataService, PdrLancamentos);

    EstoqueDataService.prototype.getServer = function() {
      return pdrServerLancamentosEstoque;
    }
    EstoqueDataService.prototype.deleteItem = function(item) {
      item = this.cleanParam(item);
      item = this.getDataById(item._id);
      this.delete(item).setTipoTexto("Item");
    }
    EstoqueDataService.prototype.criaItem = function(item, callback) {
      //Criar validação de produtos no controller
      let newData = this.createData(item);
      this.addData(newData, callback);
    }
    EstoqueDataService.prototype.getEstoque = function() {
      return this.getData();
    }
    EstoqueDataService.prototype.getEstoqueFull = function() {
      return this.getFullData();
    }
    return new EstoqueDataService();

  }]);
})();

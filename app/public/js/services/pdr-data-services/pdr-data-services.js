(function() {

  let pdrDataServices = angular.module('pdrDataServices', ['pdrDataStore', 'fmDataServices', 'pdrRelatorios']);

  pdrDataServices.run(['DataProviderService', 'ProdutosDataService', 'EstoqueDataService', 'ItensEstoqueDataService', 'VendasDataService',
  'DesperdiciosDataService', 'RelatorioVendas', 'RelatorioDesperdiciosQuantidade',
  function(DataProviderService, ProdutosDataService, EstoqueDataService, ItensEstoqueDataService, VendasDataService,
    DesperdiciosDataService, RelatorioVendas, RelatorioDesperdiciosQuantidade) {
    addProdutos(DataProviderService, ProdutosDataService);
    addEstoque(DataProviderService, EstoqueDataService);
    addItensEstoqueDataService(DataProviderService, ItensEstoqueDataService);
    addVendasDataService(DataProviderService, VendasDataService);
    addDesperdiciosDataService(DataProviderService, DesperdiciosDataService);
    addVendasRelatorioService(DataProviderService, RelatorioVendas);
  }]);

  function _customListeners(serviceName, service, DataProviderService) {
    let listener = service.listeners();

    DataProviderService.putFunctionForService(serviceName, "addListener",
    service.createCustomListener(listener.addAdicionaListener, listener), service);

    DataProviderService.putFunctionForService(serviceName, "removeAddListener",
    service.removeCustomListener(listener.removeAdicionaListener, listener), service);

    DataProviderService.putFunctionForService(serviceName, "editListener",
    service.createCustomListener(listener.addEditListener, listener), service);

    DataProviderService.putFunctionForService(serviceName, "removeEditListener",
    service.removeCustomListener(listener.removeEditListener, listener), service);

    DataProviderService.putFunctionForService(serviceName, "deleteListener",
    service.createCustomListener(listener.addDeleteListener, listener), service);

    DataProviderService.putFunctionForService(serviceName, "removeDeleteListener",
    service.removeCustomListener(listener.removeDeleteListener, listener), service);

  }

  function addProdutos(DataProviderService, ProdutosDataService) {
    DataProviderService.putNewService("Produtos");

    let listener = ProdutosDataService.listeners();
    DataProviderService.putFunctionForService("Produtos", "setEditData", ProdutosDataService.setDataToEdit, ProdutosDataService);
    DataProviderService.putFunctionForService("Produtos", "deleteData", ProdutosDataService.deleteProduto, ProdutosDataService);
    DataProviderService.putFunctionForService("Produtos", "getData", ProdutosDataService.getData, ProdutosDataService);
    DataProviderService.putFunctionForService("Produtos", "getByName", ProdutosDataService.getProdutoByNome, ProdutosDataService);
    DataProviderService.putFunctionForService("Produtos", "getById", ProdutosDataService.getProdutoById, ProdutosDataService);
    DataProviderService.putFunctionForService("Produtos", "addListener", listener.addAdicionaListener, listener);
    DataProviderService.putFunctionForService("Produtos", "removeAddListener", listener.removeAdicionaListener, listener);
    DataProviderService.putFunctionForService("Produtos", "editListener", listener.addEditListener, listener);
    DataProviderService.putFunctionForService("Produtos", "removeEditListener", listener.removeEditListener, listener);
    DataProviderService.putFunctionForService("Produtos", "deleteListener", listener.addDeleteListener, listener);
    DataProviderService.putFunctionForService("Produtos", "removeDeleteListener", listener.removeDeleteListener, listener);
  }

  function addEstoque(DataProviderService, EstoqueDataService) {
    DataProviderService.putNewService("Estoque");

    DataProviderService.putFunctionForService("Estoque", "setEditData", EstoqueDataService.setDataToEdit, EstoqueDataService);
    DataProviderService.putFunctionForService("Estoque", "deleteData", EstoqueDataService.deleteItem, EstoqueDataService);
    DataProviderService.putFunctionForService("Estoque", "getData", EstoqueDataService.getEstoque, EstoqueDataService);
    _customListeners("Estoque", EstoqueDataService, DataProviderService);
  }

  function addItensEstoqueDataService(DataProviderService, ItensEstoqueDataService) {
    DataProviderService.putNewService("ItensEstoque");

    DataProviderService.putFunctionForService("ItensEstoque", "getData", ItensEstoqueDataService.getItens, ItensEstoqueDataService);
  }

  function addVendasDataService(DataProviderService, VendasDataService) {
    DataProviderService.putNewService("Venda");

    let listener = VendasDataService.listeners();
    DataProviderService.putFunctionForService("Venda", "setEditData", VendasDataService.setDataToEdit, VendasDataService);
    DataProviderService.putFunctionForService("Venda", "deleteData", VendasDataService.deleteSale, VendasDataService);
    DataProviderService.putFunctionForService("Venda", "getData", VendasDataService.getVendas, VendasDataService);
    DataProviderService.putFunctionForService("Venda", "getFullData", VendasDataService.getVendasFull, VendasDataService);
    _customListeners("Venda", VendasDataService, DataProviderService);
  }

  function addDesperdiciosDataService(DataProviderService, DesperdiciosDataService) {
    DataProviderService.putNewService("Desperdicio");

    let listener = DesperdiciosDataService.listeners();
    DataProviderService.putFunctionForService("Desperdicio", "setEditData", DesperdiciosDataService.setDataToEdit, DesperdiciosDataService);
    DataProviderService.putFunctionForService("Desperdicio", "deleteData", DesperdiciosDataService.deleteWaste, DesperdiciosDataService);
    DataProviderService.putFunctionForService("Desperdicio", "getData", DesperdiciosDataService.getDesperdicios, DesperdiciosDataService);
    DataProviderService.putFunctionForService("Desperdicio", "getFullData", DesperdiciosDataService.getDesperdiciosFull, DesperdiciosDataService);
    _customListeners("Desperdicio", DesperdiciosDataService, DataProviderService);
  }

  // Deixar isso aqui para que o o RelatorioVendas seja inicializado junto com a aplicação, até que a table possa fazer o carregamento
  // dos serviços de forma assincrona
  function addVendasRelatorioService(DataProviderService, RelatorioVendas) {
  }

})();

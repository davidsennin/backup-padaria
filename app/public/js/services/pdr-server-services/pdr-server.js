(function() {

  let padarias = angular.module('padarias');

  padarias.factory('pdrMakeServer', [function() {

    function makeServer(resourceSemId, resourceComId) {
      return {
        listar: function() {
          return resourceSemId.query().$promise;
        },
        criar: function(item) {
          if (item._id) {
            return resourceComId.update({id: item._id}, item).$promise;
          } else {
            return resourceSemId.save(item).$promise;
          }
        },
        deleta: function(item) {
          return resourceComId.delete({id: item._id}).$promise;
        }
      };
    }

    return makeServer;

  }]);

})();

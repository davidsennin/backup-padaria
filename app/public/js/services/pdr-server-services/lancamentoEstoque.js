(function() {

  let padarias = angular.module('padarias');

  padarias.factory('pdrServerLancamentosEstoque', ['$resource', 'pdrMakeServer', function($resource, pdrMakeServer) {

    let lancamentosEstoque = $resource('/lancamento-estoque', null, {});

    let lancamentoEstoque = $resource('/lancamento-estoque/:id', null, {
      'update': {
        method: 'PUT'
      }
    });


    return pdrMakeServer(lancamentosEstoque, lancamentoEstoque);

  }]);

})();

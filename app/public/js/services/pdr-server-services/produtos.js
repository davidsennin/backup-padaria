(function() {

  let padarias = angular.module('padarias');

  padarias.factory('pdrServerProdutos', ['$resource', 'pdrMakeServer', function($resource, pdrMakeServer) {

    let produtos = $resource('/produto', null, {});

    let produto = $resource('/produto/:id', null, {
      'update': {
        method: 'PUT'
      }
    });

    return pdrMakeServer(produtos, produto);

  }]);

})();

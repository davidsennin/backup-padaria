(function() {

  let padarias = angular.module('padarias');

  padarias.factory('pdrServerLancamentosDesperdicio', ['$resource', 'pdrMakeServer', function($resource, pdrMakeServer) {

    let lancamentosDesperdicio = $resource('/lancamento-desperdicio', null, {});

    let lancamentoDesperdicio = $resource('/lancamento-desperdicio/:id', null, {
      'update': {
        method: 'PUT'
      }
    });

    return pdrMakeServer(lancamentosDesperdicio, lancamentoDesperdicio);

  }]);

})();

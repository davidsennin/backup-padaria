(function() {

  let padarias = angular.module('padarias');

  padarias.factory('pdrServerLancamentosVenda', ['$resource', 'pdrMakeServer', function($resource, pdrMakeServer) {

    let lancamentosVenda = $resource('/lancamento-venda', null, {});

    let lancamentoVenda = $resource('/lancamento-venda/:id', null, {
      'update': {
        method: 'PUT'
      }
    });

    return pdrMakeServer(lancamentosVenda, lancamentoVenda);

  }]);

})();

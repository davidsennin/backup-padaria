(function() {

  let fmDataServices = angular.module('fmDataServices', []);

  fmDataServices.factory('DataProviderService', ['$q', '$timeout', function($q, $timeout) {
    let serviceList = {};
    let asyncGetters = {};

    // Será que vai ficar pesadão?
    $timeout(_asyncCheck, 1000);

    function _asyncCheck() {
      _resolveAsync();
      $timeout(_asyncCheck, 1000);
    }

    function _findInKeys(key, keys) {
      for (let i = 0; i < keys.length; i++) {
        if (key == keys[i]) {
          return true;
        }
      }
      return false;
    }

    function _removePromisse(keys) {
      let ks = Object.keys(asyncGetters);
      let as = {};
      for (let i = 0; i < ks.length; i++) {
        let k = ks[i];
        if (!_findInKeys(k, keys)) {
          as[k] = asyncGetters[k];
        }
      }

      asyncGetters = as;
    }

    function _resolveAsyncPromisse(d, key) {
      let service = _getService(key);
      d.resolve(service);
    }

    function _testKey(key, resolved) {
      if (_hasService(key)) {
        let promises = asyncGetters[key];
        for (let i = 0; i < promises.length; i++) {
          let p = promises[i];
          if (p) {
            _resolveAsyncPromisse(p, key);
            resolved.push(key);
          }
        }
      }
      // let d = asyncGetters[key];
      // if (d) {
      //   if (_hasService(key)) {
      //     _resolveAsyncPromisse(d, key);
      //     resolved.push(key);
      //   }
      // }
    }

    function _resolveAsync() {
      let keys = Object.keys(asyncGetters);
      let resolved = [];
      for (i = 0; i < keys.length; i++) {
        key = keys[i];
        _testKey(key, resolved);
      }
      _removePromisse(resolved);
    }

    function _hasService(serviceName) {
      return angular.isDefined(serviceList[serviceName]);
    }

    function _getService(serviceName) {
      if (!_hasService(serviceName)) {
        throw new Error(`Não foi encontrado o servico de nome ${serviceName}`);
      }
      return serviceList[serviceName];
    }

    function _getServicePromisse(name) {
      let deferred = $q.defer();
      if (asyncGetters[name] == undefined) {
        asyncGetters[name] = [];
      }
      asyncGetters[name].push(deferred);
      return deferred.promise;
    }

    function _createFunction(func, own) {
      return {fn: func, owner: own};
    }

    function Service(service, functions) {
      this.service = service;
      this.functions = functions;
    }
    Service.prototype.safeRun = function(functionName, ...params) {
      try {
        let fn = this.functions[functionName];
        let func = fn.fn;
        let owner = fn.owner;
        let a = func.call(owner, params);
        return a;
      } catch(err) {
        // console.log(`Função ${functionName} não está cadastrada para o serviço`);
        console.log(err);
      }
    }

    let services = {};

    services.putNewService = function(name) {
      if (_hasService(name)) {
        throw new Error(`O servico ${name} já está cadastrado`);
      }
      let functions = {
        addData: _createFunction(undefined, undefined),
        setEditData: _createFunction(undefined, undefined),
        deleteData: _createFunction(undefined, undefined),
        addListener: _createFunction(undefined, undefined),
        removeAddListener: _createFunction(undefined, undefined),
        editListener: _createFunction(undefined, undefined),
        removeEditListener: _createFunction(undefined, undefined),
        deleteListener: _createFunction(undefined, undefined),
        removeDeleteListener: _createFunction(undefined, undefined)
      };
      let s = new Service(name, functions);
      serviceList[name] = s;
    }

    services.putFunctionForService = function(serviceName, functionName, fn, owner) {
      let service = _getService(serviceName);
      if (!angular.isFunction(fn)) {
        throw new Error(`O parâmetro fn em putFunctionForService deve ser uma função válida ${serviceName}`);
      }
      let func = _createFunction(fn, owner);
      service.functions[functionName] = func;
    }

    services.getService = function(name) {
      return _getService(name);
    }

    services.getServicePromisse = function(name) {
      return _getServicePromisse(name);
    }

    services.getFunction = function(serviceName, functionName) {
      let service = _getService(serviceName);
      return service.functions[functionName];
    }

    return services;

  }]);

})();

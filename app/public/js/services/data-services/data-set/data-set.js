(function() {

  let dataSet = angular.module('fmDataSet', ['fmDiversos']);

  dataSet.factory('FMDataSet', ['DataSetListenerManager', 'normalizeParam',
  function(DataSetListenerManager, normalizeParam) {
    let FMDataSet = function() {
      this._data = {};
      this._listeners = new DataSetListenerManager();
    }
    let proto = FMDataSet.prototype;

    proto.addData = function(data) {
      data = normalizeParam(data);
      let copy = angular.copy(data);
      if (!copy._id) {
        throw new Error("Dataset não armazena valores sem o campo _id");
      }
      let lis;
      // Se for undefined, ainda não adicionou algum elemento com esse id
      if (!this._data[copy._id]) {
        lis = this._listeners.notifyAdicionaListeners.bind(this._listeners);
      } else {
        lis = this._listeners.notifyEditListeners.bind(this._listeners);
      }
      this._data[copy._id] = copy;
      lis(data);
    }

    proto.deleteData = function(data) {
      data = normalizeParam(data);
      if (!data._id || !this._data[data._id]) {
        throw new Error('Impossível remover um elemento que não tenha sido previamente cadastrado');
      }
      try {
        delete this._data[data._id];
      } catch (e) {
        // Só levanta a merda
        throw e;
      } finally {
        this._listeners.notifyDeleteListeners(data);
      }
    }

    proto.listeners = function() {
      return this._listeners;
    }

    function makeArray(associative) {
      let arr = [];
      for (key in associative) {
        arr.push(associative[key]);
      }
      return arr;
    }

    proto.getData = function() {
      return makeArray(this._data);
    }

    proto.getDataBy = function(key, value) {
      let searchValue;
      if (angular.isObject(value)) {
        searchValue = value[key]
      } else {
        searchValue = value;
      }
      if (key == "_id") {
        // Que bagunça isso de não definir um padrão
        return this._data[searchValue]
      } else {
        let arr = makeArray(this._data);

        return arr.find(data => {
          return data[key] == searchValue;
        });
      }
    }

    return FMDataSet;

  }]);

  dataSet.factory('DataSetListenerManager', ['ListenerManager', function(ListenerManager) {

    function DataSetListenerManager() {
      this._addListener = new ListenerManager();
      this._editListener = new ListenerManager();
      this._removeListener = new ListenerManager();
    }

    let _notifyAll = function(list, ...data) {
      list.notifyAll(...data);
    }

    DataSetListenerManager.prototype.addAdicionaListener = function(fn) {
      this._addListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeAdicionaListener = function(fn) {
      this._addListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyAdicionaListeners = function(...data) {
      _notifyAll(this._addListener, data);
    }

    DataSetListenerManager.prototype.addEditListener = function(fn) {
      this._editListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeEditListener = function(fn) {
      this._editListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyEditListeners = function(...data) {
      _notifyAll(this._editListener, data);
    }

    DataSetListenerManager.prototype.addDeleteListener = function(fn) {
      this._removeListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeDeleteListener = function(fn) {
      this._removeListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyDeleteListeners = function(...data) {
      _notifyAll(this._removeListener, data);
    }

    return DataSetListenerManager;

  }]);

})();

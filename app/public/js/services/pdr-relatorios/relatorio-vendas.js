(function() {

  let pdrRelatorios = angular.module('pdrRelatorios');

  pdrRelatorios.factory('RelatorioVendas', ['numberFilter', 'PdrRelatorios',
  function(numberFilter, PdrRelatorios) {

    class RelatorioVendas extends PdrRelatorios{

      constructor() {
        super("Venda", "getFullData");
      }

      _createService(provider) {
        provider.putNewService("RelatorioVenda");
        provider.putFunctionForService("RelatorioVenda", "getData", this.getData, this);
      }

    }

    return new RelatorioVendas();

  }]);

})();

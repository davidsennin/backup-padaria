(function() {

  let pdrCommon = angular.module('padarias');

  pdrCommon.factory('commonController', ['normalizeParam', function(normalizeParam) {

    function CommonController($scope, service, form) {
      this._scope = $scope;
      this._service = service;
      this._form = form;
      this._initialize();
    }

    function _elementBlur(event) {
      let fieldName = event.target.name;
      let blurFn = fieldName + "Blur";

      // O owner dessa função será o escopo, portanto podemos verificar a existencia da função diretamente nele
      let fn = this[blurFn];
      if (fn) {
        fn(event.target.value);
      }
    }

    CommonController.prototype._initialize = function() {
      let self = this;
      this._service.putEditSetListener(function(data) {
        if (self._scope.receiveEditItem) {
          self._scope.receiveEditItem(normalizeParam(data));
          self._setEdicao();
        }
      });
      this._setCadastro();
      this._scope.onBlur = _elementBlur;
      this._scope.dataAdded = false;
    }

    CommonController.prototype._setMode = function(adicionaText, limpaText, mode) {
      this._scope.adicionaText = "Editar";
      this._scope.limparText = "Cancelar";

      this._scope.modoEditar = mode;
      this._scope.$broadcast('padaria', 'foca-nome');
    }

    CommonController.prototype._setEdicao = function() {
      this._setMode("Editar", "Cancelar", true);
      this._scope.dataAdded = false;
    }

    CommonController.prototype._setCadastro = function() {
      this._setMode("Cadastrar", "Limpar", false);
    }

    CommonController.prototype.isEditMode = function() {
      return this._scope.modoEditar;
    }

    CommonController.prototype._limpar = function() {
      // this._scope.produto = {};
      this._scope.newData();
      this._scope[this._form].$setPristine();
      this._scope.$broadcast('padaria', 'foca-nome');
      this._scope.errors = [];
      this._scope.insertError = false;

    }

    CommonController.prototype.cancel = function() {
      this._setCadastro();
      this._limpar();
    }

    CommonController.prototype.insertCallback = function(error) {
      if (error) {
        _insertFail.call(this, error);
      } else {
        _insertSuccess.call(this);
      }
    }

    function _insertSuccess() {
      this._scope.dataAdded = true;
      this._setCadastro();
      this._limpar();
      this._scope.$broadcast('fm-event', 'clear-form');
    }

    function _insertFail(error) {
      this._scope.insertError = true;
      //Se foi erro interno do servidor, virá no formato html
      if (error.status == 500) {
        this._scope.errors = [{message: error.data}];
      } else
      //Se for 400, é algum erro tratado pelo server, então virá como json
      if (error.status == 400) {
        this._scope.errors = error.data;
      }
    }

    CommonController.prototype.createData = function() {
      this._scope.dataAdded = false;
      this._scope.insertError = false;
      if (this._scope.create) {
        if (this._scope[this._form].$valid) {
          this._scope.create();
        }
      }
    }

    return CommonController;

  }]);

})()

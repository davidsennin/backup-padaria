(function() {

  let pdrCommonLanc = angular.module('padarias');

  pdrCommonLanc.factory('commonLancamentoController', ['commonController', 'makeInherit', 'pdrConfimDeletePanel', 'ProdutosDataService',
  function(commonController, makeInherit, pdrConfimDeletePanel, ProdutosDataService) {

    function CommonLancamentoController($scope, service, form) {
      commonController.call(this, $scope, service, form);
      this._initializeLancamentos();
    }
    makeInherit(CommonLancamentoController, commonController);

    CommonLancamentoController.prototype._confirmSearch = function() {
      let self = this;
      return function(item) {
        if (self._scope.setProdutoSearch) {
          let prod = ProdutosDataService.getProdutoById(item);
          self._scope.setProdutoSearch(prod.nome);
        }
        console.log("Data?");
        self._scope.$broadcast('padaria', 'foca-data');
      }
    }
    CommonLancamentoController.prototype._cancelSearch = function() {
      let self = this;
      return function() {
        if (self._scope.cancelaBusca) {
          self._scope.cancelaBusca();
        }
        self._scope.$broadcast('padaria', 'foca-nome');
      }
    }

    CommonLancamentoController.prototype._initializeLancamentos = function() {
      let self = this;
      this._scope.openSearchProduto = function() {
        pdrConfimDeletePanel.openSearchProdutoPanel(self._confirmSearch(), self._cancelSearch());
      }
    }


    return CommonLancamentoController;

  }]);

})();

(function () {

  let fmMasks = angular.module('fmMasks');

  fmMasks.directive('fmHourMask', [function() {

    let hourMask = {};

    hourMask.require = "^ngModel";
    hourMask.strict = "A";
    hourMask.scope = {
      data: '='
    }

    hourMask.compile = function(tElement, tAttr) {

      return {
        pre: function(scope, iElement, iAttr, ngModel) {

        },
        post: function(scope, iElement, iAttr, ngModel) {

          function shiftLeft(value) {
            // let dotPos = value.indexOf(":");
            value = value.substring(1,2) + value.substring(3,4) + ":" + value.substring(4);
            return value;
          }

          function shiftRight(value) {
            value = "0" + value.substring(0,1) + ":" + value.substring(1).replace(":", "");
            return value;
          }

          function parseValue(value) {
            if (value.length > 5) {
              return shiftLeft(value);
            } else
            if (value.length < 5){
              return shiftRight(value);
            }
          }

          function parser(value) {
            if (!value) {
              return value;
            }

            if (value == "00:0") {
              value = "00:00";
            }

            value = parseValue(value);
            ngModel.$viewValue = value;
            ngModel.$render();
            return value;
          }

          // Será executado toda vez que o valor do campo binded ao ngModel for alterado
          // via atribuição pelo escopo (scope.value = newValue)
          function formatter(value) {
            if (!value) {
              return value;
            }
            ngModel.$modelValue = value;
            return value;
          }

          ngModel.$parsers.push(parser);
          ngModel.$formatters.push(formatter);

          let charCount = 0;

          iElement.on('keypress', function(event) {
            if (charCount == 4 || Number.isNaN(event.key)) {
              event.preventDefault();
            } else {
              charCount++;
            }
          });

          iElement.on('keydown', function(event) {
            if (event.key == "Backspace") {
              if (charCount > 0) {
                charCount--;
              }
            }
          });

          iElement.on('focus', function(event) {
            if (this.setSelectionRange) {
              let length = this.value.length;
              this.setSelectionRange(length, length);
            }
          });

          scope.data = "00:00";
          scope.$watch('data', function(newV, oldV) {
            if (!newV) {
              scope.data = "00:00";
            }
          })
        }
      }

    }

    return hourMask;

  }]);

})();


let logger;

function validaProduto(request, response, next) {
  logger.info("Iniciando validação dos campos de produto para a requisição " + request.originalUrl);

  request.assert("nome", "O nome do produto deve ser informado").notEmpty().len(1, 255);
  request.assert("custo", "O custo do produto deve ser informado e ser um decimal válido, com duas casas decimais")
    .notEmpty().isFloat();
  request.assert("preco", "O preço do produto deve ser informado e ser um decimal válido, com duas casas decimais")
    .notEmpty().isFloat();

  let errors = request.validationErrors();
  if (errors) {
    logger.error("Ocorreram erros ao validar os campos da requisição " + request.originalUrl);
    response.status(400).send(errors);
  } else {
    logger.info("     -- Finalizada a validação --      ");
    next();
  }
}


module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando validadores/produtos");

  return validaProduto;
}

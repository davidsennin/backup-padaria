
let logger;

function validaEstoque(request, response, next) {
  logger.info("Iniciando validação dos campos de estoque para a requisição " + request.originalUrl);

  request.assert("produto_id", "O código do produto deve ser informado").notEmpty();
  request.assert("data", "A data do lançamento deve ser informada e deve ser uma data válida").toDate().notEmpty();
  // request.assert("data", "A data do lançamento deve ser informada e deve ser uma data válida")
  //   .notEmpty().isDate();
  request.assert("quantidade", "A quantidade do lançamento deve ser informada e deve ser um inteiro válido")
    .notEmpty().isInt();

  let errors = request.validationErrors();
  if (errors) {
    logger.error("Ocorreram erros ao validar os campos da requisição " + request.originalUrl);
    logger.error(JSON.stringify(errors));
    response.status(400).json(errors);
  } else {
    logger.info("     -- Finalizada a validação --      ");
    next();
  }
}


module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando validadores/produtos");

  return validaEstoque;
}

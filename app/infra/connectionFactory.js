let mysql = require("mysql");

function c(app) {
  return function connectionDBFactory() {
    let connection;
    if (!process.env.NODE_ENV || process.env.NODE_ENV == 'production') {
      connection = mysql.createConnection({
        // host: 'localhost',
        host: app.config.database.ip,
        user: 'root',
        password: 'root',
        database: 'padarias_cadastros'
      });
    }
    if (process.env.NODE_ENV == 'test') {
      connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'padarias_cadastros_test'
      });
    }
    return connection;
  }
  
}
// function connectionDBFactory() {
//   let connection;
//   if (!process.env.NODE_ENV || process.env.NODE_ENV == 'production') {
//     connection = mysql.createConnection({
//       // host: 'localhost',
//       host:process.env.DATABASE_SERVER.ip,
//       user: 'root',
//       password: 'root',
//       database: 'padarias_cadastros'
//     });
//   }
//   if (process.env.NODE_ENV == 'test') {
//     connection = mysql.createConnection({
//       host: 'localhost',
//       user: 'root',
//       password: '',
//       database: 'padarias_cadastros_test'
//     });
//   }
//   return connection;
// }

module.exports = function(app) {
  // return connectionDBFactory;
  return c(app);
}

function EstoqueDAO(connection) {
  this._connection = connection;
}

EstoqueDAO.prototype.listaLancamentosEstoque = function(callback) {
  this._connection.query('select * from lancamento_estoque', callback);
}

EstoqueDAO.prototype.insereLancamentosEstoque = function(lancamento, callback) {
  this._connection.query('insert into lancamento_estoque set ?', lancamento, callback);
}

EstoqueDAO.prototype.atualiza = function(lancamento, callback) {
  this._connection.query('update lancamento_estoque set ? where _id = ?', [lancamento, lancamento._id], callback);
}

EstoqueDAO.prototype.deleta = function(id, callback) {
  this._connection.query('delete from lancamento_estoque where _id = ?', id, callback);
}

EstoqueDAO.prototype.quantidadeProdutosLancados = function(produto_id, callback) {
  this._connection.query('select count(*) from lancamento_estoque where produto_id = ?', produto_id, callback);
}

EstoqueDAO.prototype.listaLancamentoEstoqueId = function(id, callback) {
  this._connection.query('select * from lancamento_estoque where _id = ?', id, callback);
}

module.exports = function(app) {
  return EstoqueDAO;
}

function LancamentosDAO(connection) {
  this._connection = connection;
}

LancamentosDAO.prototype.listaLancamentosProdutoIdApartirData = function(prod_id, apartir, callback) {
  this._connection.query(`(select sum(quantidade), 'E' as tipo, data from lancamento_estoque
                          where produto_id=? and data>=? group by data order by data)

                          union

                          (select sum(quantidade), 'V' as tipo, data from lancamento_venda
                          where produto_id=? and data>=? group by data)

                          union

                          (select sum(quantidade), 'D' as tipo, data from lancamento_desperdicio
                          where produto_id=? and data>=? group by data)`,
                        [prod_id, apartir, prod_id, apartir, prod_id, apartir], callback);
}

LancamentosDAO.prototype.listaLancamentosProdutoIdAntesData = function(prod_id, antes, callback) {
  try {
    this._connection.query(`(select sum(quantidade), 'E' as tipo, data from lancamento_estoque
                            where produto_id=? and data<=? group by data order by data)

                            union

                            (select sum(quantidade), 'V' as tipo, data from lancamento_venda
                            where produto_id=? and data<=? group by data)

                            union

                            (select sum(quantidade), 'D' as tipo, data from lancamento_desperdicio
                            where produto_id=? and data<=? group by data)`,
                          [prod_id, antes, prod_id, antes, prod_id, antes], callback);

  } catch (e) {
    console.log(e);
  }
}

LancamentosDAO.prototype.listaLancamentosProdutoIdTodasDatas = function(prod_id, antes, callback) {
  try {
    this._connection.query(`(select sum(quantidade), 'E' as tipo, data from lancamento_estoque
                            where produto_id=? group by data order by data)

                            union

                            (select sum(quantidade), 'V' as tipo, data from lancamento_venda
                            where produto_id=? group by data)

                            union

                            (select sum(quantidade), 'D' as tipo, data from lancamento_desperdicio
                            where produto_id=? group by data)`,
                          [prod_id, prod_id, prod_id], callback);

  } catch (e) {
    console.log(e);
  }
}


module.exports = function(app) {
  return LancamentosDAO;
}

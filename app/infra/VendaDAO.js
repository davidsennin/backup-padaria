function VendaDAO(connection) {
  this._connection = connection;
}

VendaDAO.prototype.listaLancamentosVenda = function(callback) {
  this._connection.query('select * from lancamento_venda', callback);
}

VendaDAO.prototype.insereLancamentosVenda = function(lancamento, callback) {
  this._connection.query('insert into lancamento_venda set ?', lancamento, callback);
}

VendaDAO.prototype.atualiza = function(lancamento, callback) {
  this._connection.query('update lancamento_venda set ? where _id = ?', [lancamento, lancamento._id], callback);
}

VendaDAO.prototype.deleta = function(id, callback) {
  this._connection.query('delete from lancamento_venda where _id = ?', id, callback);
}

VendaDAO.prototype.listaLancamentosVendaId = function(id, callback) {
  this._connection.query('select * from lancamento_venda where _id = ?', id, callback);
}


module.exports = function(app) {
  return VendaDAO;
}

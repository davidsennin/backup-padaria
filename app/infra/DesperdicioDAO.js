function DesperdicioDAO(connection) {
  this._connection = connection;
}

DesperdicioDAO.prototype.listaLancamentosDesperdicio = function(callback) {
  this._connection.query('select * from lancamento_desperdicio', callback);
}

DesperdicioDAO.prototype.insereLancamentosDesperdicio = function(lancamento, callback) {
  this._connection.query('insert into lancamento_desperdicio set ?', lancamento, callback);
}

DesperdicioDAO.prototype.atualiza = function(lancamento, callback) {
  this._connection.query('update lancamento_desperdicio set ? where _id = ?', [lancamento, lancamento._id], callback);
}

DesperdicioDAO.prototype.deleta = function(id, callback) {
  this._connection.query('delete from lancamento_desperdicio where _id = ?', id, callback);
}

DesperdicioDAO.prototype.listaLancamentosDesperdicioId = function(id, callback) {
  this._connection.query('select * from lancamento_desperdicio where _id = ?', id, callback);
}


module.exports = function(app) {
  return DesperdicioDAO;
}

function ProdutosDAO(connection) {
  this._connection = connection;
}

ProdutosDAO.prototype.listaProdutos = function(callback) {
  this._connection.query('select * from produtos', callback);
}

ProdutosDAO.prototype.listaProduto = function(id, callback) {
  this._connection.query('select * from produtos where _id = ?', id, callback);
}

ProdutosDAO.prototype.insereProduto = function(produto, callback) {
  this._connection.query('insert into produtos set ?', produto, callback);
}

ProdutosDAO.prototype.atualiza = function(produto, callback) {
  this._connection.query('update produtos set ? where _id = ?', [produto, produto._id], callback);
}

ProdutosDAO.prototype.deleta = function(id, callback) {
  this._connection.query('delete from produtos where _id = ?', id, callback);
}

module.exports = function(app) {
  return ProdutosDAO;
}

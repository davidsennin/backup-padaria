
module.exports = function(app) {
  let lancamentosEstoque = app.controllers.lancamentoEstoque;
  let validador = app.validadores.lancamentoEstoque;

  app.route('/lancamento-estoque')
    .get(lancamentosEstoque.lista)
    .post(validador)
    .post(lancamentosEstoque.cria);

  app.route('/lancamento-estoque/:id')
    .get(lancamentosEstoque.listaId)
    .put(validador)
    .put(lancamentosEstoque.atualiza)
    .delete(lancamentosEstoque.deleta);

}

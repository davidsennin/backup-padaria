
module.exports = function(app) {
  let produtos = app.controllers.produtos;
  let validador = app.validadores.produtos;

  app.route('/produto')
    .get(produtos.lista)
    .post(validador)
    .post(produtos.cria);

  app.route('/produto/:id')
    .get(produtos.listaId)
    .put(validador)
    .put(produtos.atualiza)
    .delete(produtos.deleta);

}


module.exports = function(app) {
  let lancamentoDesperdicio = app.controllers.lancamentoDesperdicio;
  let validador = app.validadores.lancamentoEstoque;

  app.route('/lancamento-desperdicio')
    .get(lancamentoDesperdicio.lista)
    .post(validador)
    .post(lancamentoDesperdicio.cria);

  app.route('/lancamento-desperdicio/:id')
    .get(lancamentoDesperdicio.listaId)
    .put(validador)
    .put(lancamentoDesperdicio.atualiza)
    .delete(lancamentoDesperdicio.deleta);

}

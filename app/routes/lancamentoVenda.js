
module.exports = function(app) {
  let lancamentosVenda = app.controllers.lancamentoVenda;
  let validador = app.validadores.lancamentoEstoque;

  app.route('/lancamento-venda')
    .get(lancamentosVenda.lista)
    .post(validador)
    .post(lancamentosVenda.cria);

  app.route('/lancamento-venda/:id')
    .get(lancamentosVenda.listaId)
    .put(validador)
    .put(lancamentosVenda.atualiza)
    .delete(lancamentosVenda.deleta);

}

function LancamentosDesperdicio() {
}

let lancamentosDesperdicio = {}, common, logger;

LancamentosDesperdicio.prototype.lista = function(request, response, next) {
  common.lista(request, response, next, lancamentosDesperdicio.getLancamentosDesperdicio.bind(lancamentosDesperdicio));
}

LancamentosDesperdicio.prototype.listaId = function(request, response, next) {

}

LancamentosDesperdicio.prototype.cria = function(request, response, next) {
  common.cria(request, response, next, lancamentosDesperdicio.criaLancamentoDesperdicio.bind(lancamentosDesperdicio));
}

LancamentosDesperdicio.prototype.deleta = function(request, response, next) {
  common.deleta(request, response, next, lancamentosDesperdicio.deletaLancamentoDesperdicio.bind(lancamentosDesperdicio));
}

LancamentosDesperdicio.prototype.atualiza = function(request, response, next) {
  common.atualiza(request, response, next, lancamentosDesperdicio.atualizaLancamentoDesperdicio.bind(lancamentosDesperdicio));
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando controllers/lancamentosDesperdicio");

  lancamentosDesperdicio = app.api.lancamentoDesperdicio;
  common = app.controllers.common("lançamento de desperdicio");
  return new LancamentosDesperdicio();
}

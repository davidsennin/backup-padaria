
function LancamentosEstoque() {
}

let lancamentosEstoque = {}, common, logger;

LancamentosEstoque.prototype.lista = function(request, response, next) {
  common.lista(request, response, next, lancamentosEstoque.getLancamentosEstoque.bind(lancamentosEstoque));
}

LancamentosEstoque.prototype.listaId = function(request, response, next) {

}

LancamentosEstoque.prototype.cria = function(request, response, next) {
  common.cria(request, response, next, lancamentosEstoque.criaLancamentoEstoque.bind(lancamentosEstoque));
}

LancamentosEstoque.prototype.deleta = function(request, response, next) {
  common.deleta(request, response, next, lancamentosEstoque.deletaLancamentoEstoque.bind(lancamentosEstoque));
}

LancamentosEstoque.prototype.atualiza = function(request, response, next) {
  common.atualiza(request, response, next, lancamentosEstoque.atualizaLancamentoEstoque.bind(lancamentosEstoque));
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando controllers/lancamentoEstoque");

  lancamentosEstoque = app.api.lancamentoEstoque;
  common = app.controllers.common("lançamento em estoque");
  return new LancamentosEstoque();
}

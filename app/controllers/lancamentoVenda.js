function LancamentosVenda() {
}

let lancamentoVenda = {}, common, logger;

LancamentosVenda.prototype.lista = function(request, response, next) {
  common.lista(request, response, next, lancamentoVenda.getLancamentosVenda.bind(lancamentoVenda));
}

LancamentosVenda.prototype.listaId = function(request, response, next) {

}

LancamentosVenda.prototype.cria = function(request, response, next) {
  common.cria(request, response, next, lancamentoVenda.criaLancamentoVenda.bind(lancamentoVenda));
}

LancamentosVenda.prototype.deleta = function(request, response, next) {
  common.deleta(request, response, next, lancamentoVenda.deletaLancamentoVenda.bind(lancamentoVenda));
}

LancamentosVenda.prototype.atualiza = function(request, response, next) {
  common.atualiza(request, response, next, lancamentoVenda.atualizaLancamentoVenda.bind(lancamentoVenda));
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando controllers/lancamentoVenda");

  lancamentoVenda = app.api.lancamentoVenda;
  common = app.controllers.common("lançamento de venda");
  return new LancamentosVenda();
}

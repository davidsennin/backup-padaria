
function Produtos() {
}

let produtos = {}, common, logger;

Produtos.prototype.lista = function(request, response, next) {
  common.lista(request, response, next, produtos.getProdutos.bind(produtos));
}

Produtos.prototype.listaId = function(request, response, next) {

}

Produtos.prototype.cria = function(request, response, next) {
  common.cria(request, response, next, produtos.criaProduto.bind(produtos));
}

Produtos.prototype.deleta = function(request, response, next) {
  common.deleta(request, response, next, produtos.deletaProduto.bind(produtos));
}

Produtos.prototype.atualiza = function(request, response, next) {
  common.atualiza(request, response, next, produtos.atualizaProduto.bind(produtos));
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando controllers/produtos");

  produtos = app.api.produtos;
  common = app.controllers.common("produto");
  return new Produtos();
}

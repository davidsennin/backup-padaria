

let logger;

function lista(request, response, next, getLista) {
  let self = this;
  logger.info(`Iniciando a busca da lista de ${self.msg}`);

  getLista(function(err, lista) {
    if (err) {
      logger.error(`Ocorreu um erro ao buscar a lista de ${self.msg}`);
      logger.error(JSON.stringify(err));

      return next(err);
    }
    logger.info(`Retornando lista de ${self.msg}`);
    response.json(lista);

  });
}

function cria(request, response, next, criaItem) {
  let self = this;
  let item = request.body;

  let logItem = JSON.stringify(item);
  logger.info(`Iniciando a criação do ${self.msg} ${logItem}`);

  criaItem(item, function(err, retorno) {
    if (err) {
      logger.error(`Ocorreu um erro ao criar ${self.msg} ${logItem}`);
      logger.error(JSON.stringify(err));

      if (err[0]) {
        response.status(400).json(err);
        return;
      }
      return next(err);
    }
    logger.info(`Finalizando a criação do ${self.msg} ${logItem}`);
    response.status(200).json(retorno);

  });
}

function deleta(request, response, next, deleteItem) {
  let self = this;
  let id = request.params.id;

  logger.info(`Iniciando a exclusão do ${self.msg} id = ${id}`);

  deleteItem(id, function(err, retorno) {
    if (err) {
      // Se tiver esse formato, é um erro que foi lançado internamente, pela lógica do servidor
      if (err[0]) {
        response.status(400).json(err);
        return;
      }
      return next(err);
    }
    logger.info(`Finalizando a exclusão do ${self.msg} id = ${id}`);
    response.status(204).send(retorno);
  });

}

function atualiza(request, response, next, atualizaItem) {
  let self = this;
  let id = request.params.id;
  let item = request.body;

  logger.info(`Iniciando a atualização do ${self.msg} id = ${id}`);

  atualizaItem(id, item, function(err, retorno) {
    if (err) {
      logger.error(`Ocorreu um erro na validação do ${self.msg} id = ${id}`);
      logger.error(JSON.stringify(err));

      if (err[0]) {
        console.log(err);
        response.status(400).json(err);
        return;
      }
      return next(err);
    }
    logger.info(`Finalizando a atualização do ${self.msg} id = ${id}`);
    response.send(retorno);
  });

}

function createCommon(msg) {
  return {
    msg: msg,
    lista: lista,
    cria: cria,
    deleta: deleta,
    atualiza: atualiza
  }
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando controllers/common");
  return createCommon;
}

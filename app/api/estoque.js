let logger;
let common;

const ESTOQUE = 'E', VENDA = 'V', DESPERDICIO = 'D';

function Estoque(app) {
  let infra = app.infra;
  common.call(this, infra, infra.EstoqueDAO, infra.LancamentosDAO, infra.VendaDAO, infra.DesperdicioDAO);
}

function pushData(produto, dados) {
  let data = dados.data;
  if (!produto[data]) {
    produto[data] = [];
  }
  //Não preciso totalizar, pois o select já soma todas as quantidades lançadas em uma determinada data
  //De acordo com o tipo de lançamento
  produto[data].push(dados);
}

function fazArrayLancamentosAgrupado(dadosDesagrupados, dadosAgrupados) {
  dadosDesagrupados.forEach(lancamento => {
    pushData(dadosAgrupados, lancamento);
  });
}

// Faz um sql gigante com union unindo (avá) os resultados dos lançamentos de estoque, venda e desperdício
// para o produto do item passado como parâmetro. Receberá como parâmetro também a data do item, buscando
// os lançamentos apenas a partir dela. Ta louco, fazer essa tosqueira para todos os lançamentos?
function montaEstoqueProduto(estoque, funcaoMontaEstoque, callback) {

  let result = {};
  result[estoque.produto_id] = {};

  // let lancamentosDAO = this.newDAO(1);
  // lancamentosDAO.listaLancamentosProdutoId(estoque.produto_id, estoque.data, function(err, retorno) {
  funcaoMontaEstoque(estoque.produto_id, estoque.data, function(err, retorno) {
    if (err) {
      callback(err, retorno);
      return;
    }
    let produto = result[estoque.produto_id];
    fazArrayLancamentosAgrupado(retorno, produto);
    callback(err, result);
  });

  this.close();

}

// Retorna uma Promise que executará a busca do lançamento na base de dados
// Recebe como parãmetro a funcaoDeBusca, que é a função que irá buscar o lançamento na base de dados
// É feito dessa forma para ser mais dinâmico com relação a buscas de tipos diferentes de lançamentos
function getProdutoLancamento(id, funcaoDeBusca) {
  let self = this;
  return new Promise(function(resolve, reject) {
    funcaoDeBusca(id, function(err, retorno) {
      if (err) {
        reject(err);
      } else {
        let estoque = retorno[0];
        if (!estoque) {
          reject([{message:`Não foi encontrado lançamento em estoque para o id ${id}`}]);
        } else {
          resolve(estoque);
        }
      }
    });
    self.close();
  });
}

// Totaliza o estoque para uma determinada data
function totalizaLancamentosData(dadosData) {
  let tot = 0;
  if (dadosData){
    dadosData.forEach(dados => {
      if (dados.tipo == ESTOQUE) {
        tot += dados['sum(quantidade)'];
      } else {
        tot -= dados['sum(quantidade)'];
      }
    });
  }
  return tot;
}

// Desassocia o array, ordena e re-associa
function converteDadosLancamentoParaArrayOrdenado(dadosLancamentos) {
  let res = [];
  for (data in dadosLancamentos) {
    res.push({data: data, dados: dadosLancamentos[data]});
  }
  res = res.sort(function(a, b) {
    let data1 = new Date(a.data);
    let data2 = new Date(b.data);
    if (data1.getTime() < data2.getTime()) {
      return -1;
    }
    if (data1.getTime() > data2.getTime()) {
      return 1;
    }
    return 0;
  });
  let agrupados = {};
  res.forEach((lancamento) => {
    agrupados[lancamento.data] = lancamento.dados;
  });
  return agrupados;
}

// Senhores, bem vindos ao ancapistão. Aqui receberemos, na variável lancamento, o item de lancamento que queremos
// excluir, editar ou adicionar (nos casos de vendas e desperdicios). Na variável dadosAgrupados, receberemos
// dos lançamentos para o produto do item a ser alterado/excluido/adicionado, com as suas datas e quantidades
// Eles estarão agrupados da seguinte forma: {"ID do produto": {"DATA1": [lançamentos na data 1], "DATA2": [lançamentos na data 2]}}
// E assim substantivamente. Esses valores serão usados para recalcular os estoque do produto a partir da data
// do lançamento sendo alterado/exlcuido/adicionado, de forma a ver se em alguma data haverá falta de produtos em estoque
// Por falta de produtos em estoque, entende-se estoque negativo que, caso alguém tenha inventado algo assim que seja possível,
// me avisa pra eu pode ficar milionário(e José rico). Caso fique negativa em algum ponto, notifica o client que fodeu a mariola
// Caso não fique negativo, notifica dizendo que pode sentar o aço
function trataMudancaQuantidadeItemEmEstoque(lancamento, dadosAgrupados, quantidadeMudanca, callback) {
  let dadosLancamentos = dadosAgrupados[lancamento.produto_id];
  dadosLancamentos = converteDadosLancamentoParaArrayOrdenado(dadosLancamentos);
  console.log(JSON.stringify(dadosLancamentos));
  let totalLancadoEstoque = 0;
  for (data in dadosLancamentos) {
    totalLancadoEstoque += totalizaLancamentosData(dadosLancamentos[data]);
    if (lancamento.data == data) {
      totalLancadoEstoque += quantidadeMudanca;
    }
    console.log(totalLancadoEstoque);
    if (totalLancadoEstoque < 0) {
      callback([{message:`Não foi possível realizar alteração no estoque para o lançamento id ${lancamento._id},
        pois a quantidade do produto ${lancamento.produto_id} lançado não é suficiente para isso na data ${lancamento.data}`}]);
      return;
    }
  }
  callback(undefined, lancamento.itemReal);
}

function verificaPossiblidadeExclusao(estoque, dadosAgrupados, callback) {
  let quantidade = estoque.quantidade * -1;
  trataMudancaQuantidadeItemEmEstoque(estoque, dadosAgrupados, quantidade, callback);
}

function preparaItemProcessamento(item, tipo) {
  item["sum(quantidade)"] = item.quantidade;
  item.tipo = tipo;
}

// Aqui teremos duas possibilidade: lançamento com alteração de data e sem alteração de data
// Quando não houver alteração de data, o tratamento é simples e similar ao de exclusão, sendo que,
// nesse caso, a quantidade excluida será a diferença entre o lançamento da base e o novo lançamento
// Caso haja alteração de data, ai fodeu. O valor da variável estoque será um array, sendo que a posição
// 0 é o novo valor do estoque, com a nova data e a nova quantidade, e a posição 1 será o valor antigo
// Esse dois valores serão tratados da seguinte forma: o novo valor será tratado como se fosse um novo lançamento
// O valor antigo será tratado como se fosse "venda" (diminuição do estoque). Dessa forma, no método
// trataMudancaQuantidadeItemEmEstoque, os valores em estoque serão recalculados e, caso dê merda, ai avisa
// que não vai subir ninguém
function verificaPossibilidadesEdicao(estoque, dadosAgrupados, callback) {

  if (Array.isArray(estoque)) {
    trataMudancaDataItemEstoque(estoque[0], ESTOQUE, estoque[1], VENDA, dadosAgrupados, callback);
  } else {
    let quantidade = estoque.quantidade * -1;
    // nesse caso, se a quantidade for menor que 0, quer dizer que está sendo subtraido uma determinada quantidade de itens
    // em comparação com o anterior, então deve ser feita a avalização se essa quantidade pode ser "retirada" do estoque
    // Caso seja 0 ou positiva, quer dizer que está ocorrendo um aumento da quantidade em estoque em comparação com antes
    // Então é só aprovar e partir para o abraço
    if (quantidade < 0) {
      trataMudancaQuantidadeItemEmEstoque(estoque, dadosAgrupados, quantidade, callback);
    } else {
      callback(undefined, estoque.itemReal);
    }
  }
}

function trataMudancaDataItemEstoque(itemNovo, tipoNovo, itemVelho, tipoVelho, dadosAgrupados, callback) {

  function verificaArray(item, estoque) {
    let dadoProduto = dadosAgrupados[estoque.produto_id];
    pushData(dadoProduto, estoque);
    pushData(dadoProduto, item);
    trataMudancaQuantidadeItemEmEstoque(item, dadosAgrupados, 0, callback);
  }

  preparaItemProcessamento(itemNovo, tipoNovo);
  preparaItemProcessamento(itemVelho, tipoVelho);
  verificaArray(itemNovo, itemVelho);
}

function trataLancamentosProduto(estoque, funcaoDeVerificacao, callback) {
  let self = this;
  return function (err, retorno) {
    if (err) {
      callback(err);
      return;
    }
    //Aqui temos tudo totalizado por produto, por data e por tipo de lançamento, nessa ordem de agrupamento
    //Com esses dados e com os dados do lançamento a ser excluído/editado, agora podemos verificar se a exclusão/edição deixará
    //estoque negativo em alguma data, a partir da data atual
    funcaoDeVerificacao(estoque, retorno, callback);
  }
}

Estoque.prototype.verificaExclusaoLancamentoEstoque = function (id, callback) {
  let self = this;
  // P.S: aprender como tratar exceções dentro de requisições async dentro da Promise
  // Do jeito que ta, se der merda eu to no sal, pq vai quebrar tudo e a exceção não será tratada
  let estoqueDAO = this.newDAO();
  getProdutoLancamento.call(this, id, estoqueDAO.listaLancamentoEstoqueId.bind(estoqueDAO))
    .then(function(estoque) {
      // Now i have a machine gun
      // Com o id do produto, será possível carregar os lançamentos em estoque, vendas e desperdícios
      // Com esses dados carregados, será possível calcular se a exclusão do lançamento em estoque
      // irá quebrar tudo
      let itemEstoque = fazerItemEstoque(estoque);
      let lancamentosDAO = self.newDAO(1);
      montaEstoqueProduto.call(self, itemEstoque, lancamentosDAO.listaLancamentosProdutoIdApartirData.bind(lancamentosDAO),
        trataLancamentosProduto.call(self, itemEstoque, verificaPossiblidadeExclusao, callback));
    })
    .catch(callback);
}

// Essa função irá montar o cenário para verificar se um lançamento pode ou não ser editado, baseando nas alterações,
// seja da data ou da quantidade ou de ambos. Essa função será genérica, podendo ser invocada a partir da edição de um
// lançamento em estoque, de um lançamento de venda ou de um lançamento de desperdício. Por isso, receberá como parâmetro
// a função responsável por fazer o carregamento das informações do lançamento antigo, bem como a função responsável
// por verificar a possibilidade de realizar a operação. Recebe também uma função que realizará a busca de todos os lançamentos
// necessário para essa operação. Ela é genérica porque pode ser necessário carregar os lançamentos a partir da data atual,
// para alterações de lançamentos de estoque, ou carregar os lançamentos anteriores à data atual, para os casos de alteração
// de lançamentos de venda e desperdício
function montaCenarioVerificacaoEdicaoLancamento(lancamento, funcaoBuscaLancamento, funcaoBuscaLancamentosItems,
  funcaoVerificaPossibilidade, funcaoTrataData, callback) {
  let self = this;
  let item = fazerItemEstoque(lancamento);
  getProdutoLancamento.call(this, lancamento._id, funcaoBuscaLancamento)
    .then(function(lancamentoVelho) {
      // Aqui, como temos uma alteração no lançamento, podemos verificar se houve mudança na quantidade e na data
      // Caso não, então é só partir para o abraço. Caso sim, então teremos que tratar duas situações:
      // 1-quando houver alteração na data, que é a mais complexa
      // 2-quanto houver alteração somente da quantidade, que é mais simples. Nesse caso, subtraimos a quantidade
      // do lançamento antigo pela quantidade do lançamento novo. Quando esse valor é negativo, pra o caso de estar editando
      // um lançamento em estoque, isso quer dizer que está colocando estoque à mais e se for positivo, que dizer que está
      // colocando menos itens em estoque. No caso de venda, caso seja negativo, quer dizer que está vendendo mais, e caso seja
      // positivo, quer dizer que a venda editada foi menor
      if ((item.quantidade == lancamentoVelho.quantidade) && (item.data == lancamentoVelho.data)) {
        callback(undefined, lancamento);
      } else {
        // Se for diferente então tem que ser feito um tratamento diferenciado. Nesse caso, pode-se dizer que,
        // caso estejamos tratando um lançamento em estoque, será adicionado em estoque a quantidade do lançamento
        // vindo do cliente na data vinda do cliente e será excluída a quantidade da data do item no banco na data do item do banco
        // Do contrário, caso seja um lançamento de venda ou desperdício, será feito da forma contrária ao caso do lançamento em estoque
        if (item.data != lancamentoVelho.data) {
          // Faz isso porque, caso o lançamento do estoque no banco de dados seja de uma data inferior,
          // no caso de um lançamento em estoque, ou superior, no caso de um lançamento de venda ou desperdício, à data da alteração,
          // esse lançamento não seria considerado no cálculo do estoque (ele ficará negativo para a data, pois esse estoque
          // sendo tratado será inserido como venda). Fazendo essa alteração, o valor buscado em banco juntamente com esse valor
          // tratado se "anularão", o que é como se o dado fosse "removido" da base, e o valor alterado fosse uma nova "inserção"
          // em uma data diferente. Sendo prático, o efeito é exatamente o mesmo do que acontecerá de fato, sendo a diferença que,
          // o caso real manterá o _id, ao passo que o "teorico" trata como se lançamento atual fosse excluido e um novo desse lugar
          // a ele. Sobre a possibilidade de fazer uma chamada como item em vez de [item, estoque] para esses casos,
          // não dará certo, pois podem haver vendas entre a data anterior do estoque e a nova data que podem ocasionar um caso de
          // estoque negativo caso não seja realizado esse tratamento dessa forma
          let itemBusca = funcaoTrataData(item, lancamentoVelho);
          montaEstoqueProduto.call(self, itemBusca, funcaoBuscaLancamentosItems,
            trataLancamentosProduto.call(self, [item, lancamentoVelho], funcaoVerificaPossibilidade, callback));
        } else {
          item.quantidade = lancamentoVelho.quantidade - item.quantidade;
          montaEstoqueProduto.call(self, item, funcaoBuscaLancamentosItems,
            trataLancamentosProduto.call(self, item, funcaoVerificaPossibilidade, callback));
        }
      }
    })
    .catch(callback);
}

function trataDiferencaDataLancamentoEstoque(novo, velho) {
  if (velho.data.getTime() < novo.data.getTime()) {
    return velho;
  }
  return novo;
}

Estoque.prototype.verificaEdicaoLancamentoEstoque = function(lancamentoEstoque, callback) {
  let self = this;
  let estoqueDAO = this.newDAO();
  let lancamentosDAO = self.newDAO(1);
  montaCenarioVerificacaoEdicaoLancamento.call(this, lancamentoEstoque, estoqueDAO.listaLancamentoEstoqueId.bind(estoqueDAO),
                                               lancamentosDAO.listaLancamentosProdutoIdApartirData.bind(lancamentosDAO),
                                               verificaPossibilidadesEdicao, trataDiferencaDataLancamentoEstoque, callback);
}

function verificaPossibilidadeAdicaoVendaDesperdicio(venda, dadosAgrupados, callback) {
  preparaItemProcessamento(venda, VENDA);
  pushData(dadosAgrupados[venda.produto_id], venda);
  trataMudancaQuantidadeItemEmEstoque(venda, dadosAgrupados, 0, callback);
}

function verificaLancamentoVendaDesperdicio(lancamento, callback) {
  let item = fazerItemEstoque(lancamento);
  // Aqui não precisaremos fazer a busca do lançamento, tendo em vista que ele ainda não existe
  let lancamentosDAO = this.newDAO(1);
  montaEstoqueProduto.call(this, item, lancamentosDAO.listaLancamentosProdutoIdTodasDatas.bind(lancamentosDAO),
    trataLancamentosProduto.call(this, item, verificaPossibilidadeAdicaoVendaDesperdicio, callback));
}

Estoque.prototype.verificaLancamentoVenda = function(lancamentoVenda, callback) {
  verificaLancamentoVendaDesperdicio.call(this, lancamentoVenda, callback);
}

Estoque.prototype.verificaLancamentoDesperdicio = function(lancamentoDesperdicio, callback) {
  verificaLancamentoVendaDesperdicio.call(this, lancamentoDesperdicio, callback);
}

function verificaPossibilidadesEdicaoVendaDesperdicio(item, dadosAgrupados, callback) {

  if (Array.isArray(item)) {
    trataMudancaDataItemEstoque(item[0], VENDA, item[1], ESTOQUE, dadosAgrupados, callback);
  } else {
    let quantidade = item.quantidade;
    trataMudancaQuantidadeItemEmEstoque(item, dadosAgrupados, quantidade, callback);
  }
}

function trataDiferencaDataLancamentoVendaDesperdicio(novo, velho) {
  if (velho.data.getTime() < novo.data.getTime()) {
    return novo;
  }
  return velho;
}

Estoque.prototype.verificaEdicaoLancamentoVenda = function(lancamentoVenda, callback) {
  let self = this;
  let vendaDAO = this.newDAO(2);
  let lancamentosDAO = self.newDAO(1);
  montaCenarioVerificacaoEdicaoLancamento.call(this, lancamentoVenda, vendaDAO.listaLancamentosVendaId.bind(vendaDAO),
                                               lancamentosDAO.listaLancamentosProdutoIdTodasDatas.bind(lancamentosDAO),
                                               verificaPossibilidadesEdicaoVendaDesperdicio,
                                               trataDiferencaDataLancamentoVendaDesperdicio, callback);
}

Estoque.prototype.verificaEdicaoLancamentoDesperdicio = function(lancamentoVenda, callback) {
  let self = this;
  let desperdicioDAO = this.newDAO(3);
  let lancamentosDAO = self.newDAO(1);
  montaCenarioVerificacaoEdicaoLancamento.call(this, lancamentoVenda, desperdicioDAO.listaLancamentosDesperdicioId.bind(desperdicioDAO),
                                               lancamentosDAO.listaLancamentosProdutoIdTodasDatas.bind(lancamentosDAO),
                                               verificaPossibilidadesEdicaoVendaDesperdicio,
                                               trataDiferencaDataLancamentoVendaDesperdicio, callback);
}

function fazerItemEstoque(estoque) {
  return {_id: estoque._id, produto_id: estoque.produto_id, data: estoque.data, quantidade: estoque.quantidade, itemReal: estoque};
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/estoque");

  common = app.api['common-database-use'];
  app.services['inherit-service'].makeInherit(Estoque, common);
  return new Estoque(app);
}

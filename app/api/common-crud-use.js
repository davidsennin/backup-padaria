let logger;

function CommonCRUD(tipoMsg) {
  if (tipoMsg) {
    this._tipoMsg = tipoMsg;
  }
}

CommonCRUD.prototype.listarDadosDoBanco = function(searchFunction, callback) {
  let self = this;
  logger.info(`Realizando a busca de ${this._tipoMsg} no banco de dados`);

  searchFunction(function(err, data) {
    if (!err) {
      logger.info(`Carregou lista de ${self._tipoMsg} do banco de dados`);
    }
    callback(err, data);
  });
  this.close();
}

CommonCRUD.prototype.insereDadoNoBanco = function(item, insertFunction, callback) {
  let self = this;
  insertFunction(item, function(err, retorno) {
    if (!err) {
      logger.info(`${self._tipoMsg} inserido no banco`);
      item._id = parseInt(retorno.insertId);
    }
    callback(err, item);
  });
  this.close();
}

CommonCRUD.prototype.atualizaDadoNoBanco = function(id, item, atualizaFunction, callback) {
  let self = this;
  item._id = parseInt(id);
  atualizaFunction(item, function(err, retorno) {
    if (!err) {
      logger.info(`${self._tipoMsg} atualizado no banco`);
    }
    callback(err, item);
  });
  this.close();
}

CommonCRUD.prototype.deletaItemNoBanco = function(id, verificationFunction, deleteFunction, callback) {
  let self = this;
  let deleteCallbackFunction = function(err, retorno) {
    if (err) {
      logger.error(`Não foi possível excluir o ${self._tipoMsg} do banco de dados. ID = ${id}`);
      logger.error(JSON.stringify(err));
    } else {
      logger.info(`O ${self._tipoMsg} foi excluído da base de dados. ID = ${id}`);
    }
    callback(err, retorno);
  };
  if (verificationFunction) {
    verificationFunction(id, function(err, retorno) {
      if (!err) {
        deleteFunction(id, deleteCallbackFunction.bind(self));
      } else {
        callback(err, retorno);
      }
    });
  } else {
    deleteFunction(id, deleteCallbackFunction.bind(self));
  }
  this.close();
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/common-crud-use");

  return CommonCRUD;
}

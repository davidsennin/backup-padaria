let logger;
let common;
let commonLancamento;
let commonCrud;
let estoque;

function LancamentosDesperdicio(app) {
  let infra = app.infra;
  common.call(this, infra, infra.DesperdicioDAO, infra.ProdutosDAO);
  commonLancamento.call(this, "lançamento de desperdício");
  commonCrud.call(this);
}

LancamentosDesperdicio.prototype.getProdutoDAO = function() {
  //função implementada em common-database-use
  return this.newDAO(1);
}

LancamentosDesperdicio.prototype.getDesperdicioDAO = function() {
  //função implementada em common-database-use
  return this.newDAO();
}

LancamentosDesperdicio.prototype.getLancamentosDesperdicio = function(callback) {
  let desperdicioDAO = this.getDesperdicioDAO();
  //Função implementada em common-crud-use
  this.listarDadosDoBanco(desperdicioDAO.listaLancamentosDesperdicio.bind(desperdicioDAO), callback);
}

function validaInsercaoDesperdicio(desperdicio, callback) {
  let self = this;
  // O callback, nesse caso, será uma função anônima implementada em common-crud-use.insereDadoNoBanco, por isso passaremos
  // ela como callback do banco de dados, pois esse seria o fluxo normal caso não fosse realizada essa verificação
  estoque.verificaLancamentoDesperdicio(desperdicio, function(err, retorno) {
    if (err) {
      logger.error(`Ocorreu um erro ao validar se era possível adicionar novo lançamento de desperdicio ${JSON.stringify(desperdicio)}`);
      callback(err, undefined);
    } else {
      logger.error(`O desperdicio foi validade e será inserida no banco de dados ${JSON.stringify(desperdicio)}`);
      let desperdicioDAO = self.getDesperdicioDAO();
      desperdicioDAO.insereLancamentosDesperdicio(desperdicio, callback);
      self.close();
    }
  });
}

LancamentosDesperdicio.prototype.criaLancamentoDesperdicio = function(lancamentoDesperdicio, callback) {
  //função implementada em common-lancamento-use
  this.insereLancamentoItemNoBanco(lancamentoDesperdicio, validaInsercaoDesperdicio.bind(this), callback);
}

LancamentosDesperdicio.prototype.atualizaLancamentoDesperdicio = function(id, lancamentoDesperdicio, callback) {
  let self = this;
  //Deve fazer essa verificação, porque esse lançamento pode ter sido realizado em alguma plataforma
  //onde não é realizada a validação prévia do produto antes de enviar a alteração
  this.verificaExistenciaProdutoLancamento(lancamentoDesperdicio.produto_id, function(err, retorno) {
    if (!err) {
      estoque.verificaEdicaoLancamentoDesperdicio(lancamentoDesperdicio, function(err, retorno) {
        if (!err) {
          let desperdicioDAO = self.getDesperdicioDAO();
          //função implementada em common-crud-use
          self.atualizaDadoNoBanco(id, lancamentoDesperdicio, desperdicioDAO.atualiza.bind(desperdicioDAO), callback);
        } else {
          callback(err, retorno);
        }
      });
    } else {
      callback(err, retorno);
    }
  });

}

LancamentosDesperdicio.prototype.deletaLancamentoDesperdicio = function(id, callback) {
  let desperdicioDAO = this.getDesperdicioDAO();
  //função implementada em common-crud-use
  // O segundo parâmetro é undefind pq não precisamos fazer verificações para excluir um lançamento de venda
  this.deletaItemNoBanco(id, undefined, desperdicioDAO.deleta.bind(desperdicioDAO), callback);
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/lançamento de desperdicio");

  common = app.api['common-database-use'];
  app.services['inherit-service'].makeInherit(LancamentosDesperdicio, common);

  commonLancamento = app.api['common-lancamento-use'];
  app.services['inherit-service'].makeInherit(LancamentosDesperdicio, commonLancamento);

  commonCrud = app.api['common-crud-use'];
  app.services['inherit-service'].makeInherit(LancamentosDesperdicio, commonCrud);

  estoque = app.api.estoque;

  return new LancamentosDesperdicio(app);

}

let logger;
let common;
let estoque;
let commonLancamento;
let commonCrud;
function LancamentosEstoque(app) {
  let infra = app.infra;
  common.call(this, infra, infra.EstoqueDAO, infra.ProdutosDAO);
  commonLancamento.call(this, "lançamento em estoque");
  commonCrud.call(this);
}

LancamentosEstoque.prototype.getProdutoDAO = function() {
  //função implementada em common-database-use
  return this.newDAO(1);
}

LancamentosEstoque.prototype.getEstoqueDAO = function() {
  //função implementada em common-database-use
  return this.newDAO();
}

LancamentosEstoque.prototype.getLancamentosEstoque = function(callback) {
  let estoqueDAO = this.getEstoqueDAO();
  //função implementada em common-crud-use
  this.listarDadosDoBanco(estoqueDAO.listaLancamentosEstoque.bind(estoqueDAO), callback);
}

LancamentosEstoque.prototype.criaLancamentoEstoque = function(lancamentoEstoque, callback) {
  let estoqueDAO = this.getEstoqueDAO();
  //função implementada em common-lancamento-use
  this.insereLancamentoItemNoBanco(lancamentoEstoque, estoqueDAO.insereLancamentosEstoque.bind(estoqueDAO), callback);
}

LancamentosEstoque.prototype.atualizaLancamentoEstoque = function(id, lancamentoEstoque, callback) {
  let self = this;
  //Deve fazer essa verificação, porque esse lançamento pode ter sido realizado em alguma plataforma
  //onde não é realizada a validação prévia do produto antes de enviar a alteração
  this.verificaExistenciaProdutoLancamento(lancamentoEstoque.produto_id, function(err, retorno) {
    if (!err) {
      // TODO: Será necessário fazer uma validação da atualização do lançamento em estoque
      // Porque pode ser realizada uma alteração na quantidade de forma a deixar a quantidade negativa
      estoque.verificaEdicaoLancamentoEstoque(lancamentoEstoque, function(err, retorno) {
        if (!err) {
          let estoqueDAO = self.getEstoqueDAO();
          //função implementada em common-crud-use
          self.atualizaDadoNoBanco(id, lancamentoEstoque, estoqueDAO.atualiza.bind(estoqueDAO), callback);
        } else {
          callback(err, retorno);
        }
      });
    } else {
      callback(err, retorno);
    }
  });
}

function verificaExclusaoLancamentoEstoque(id, callback) {
  logger.info('Iniciando a verificação da possibilidade de exclusão do lançamento em estoque id = ' + id);
  // TODO: Verificar se é possível excluir o lançamento da base de dados (a quantidade restante não pode ser negativa)
  // em estoque, venda ou desperdício
  estoque.verificaExclusaoLancamentoEstoque(id, function(err, retorno) {
    callback(err, id);
  });

}

LancamentosEstoque.prototype.deletaLancamentoEstoque = function(id, callback) {
  let estoqueDAO = this.getEstoqueDAO();
  //função implementada em common-crud-use
  this.deletaItemNoBanco(id, verificaExclusaoLancamentoEstoque.bind(this), estoqueDAO.deleta.bind(estoqueDAO), callback);
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/lançamento estoque");

  common = app.api['common-database-use'];
  app.services['inherit-service'].makeInherit(LancamentosEstoque, common);

  commonLancamento = app.api['common-lancamento-use'];
  app.services['inherit-service'].makeInherit(LancamentosEstoque, commonLancamento);

  commonCrud = app.api['common-crud-use'];
  app.services['inherit-service'].makeInherit(LancamentosEstoque, commonCrud);

  estoque = app.api.estoque;

  return new LancamentosEstoque(app);

}

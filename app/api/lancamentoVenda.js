let logger;
let common;
let commonLancamento;
let commonCrud;
let estoque;

function LancamentosVenda(app) {
  let infra = app.infra;
  common.call(this, infra, infra.VendaDAO, infra.ProdutosDAO);
  commonLancamento.call(this, "lançamento de venda");
  commonCrud.call(this);
}

LancamentosVenda.prototype.getProdutoDAO = function() {
  //função implementada em common-database-use
  return this.newDAO(1);
}

LancamentosVenda.prototype.getVendaDAO = function() {
  //função implementada em common-database-use
  return this.newDAO();
}

LancamentosVenda.prototype.getLancamentosVenda = function(callback) {
  let vendaDAO = this.getVendaDAO();
  //Função implementada em common-crud-use
  this.listarDadosDoBanco(vendaDAO.listaLancamentosVenda.bind(vendaDAO), callback);
}

function validaInsercaoVenda(venda, callback) {
  let self = this;
  // O callback, nesse caso, será uma função anônima implementada em common-crud-use.insereDadoNoBanco, por isso passaremos
  // ela como callback do banco de dados, pois esse seria o fluxo normal caso não fosse realizada essa verificação
  estoque.verificaLancamentoVenda(venda, function(err, retorno) {
    if (err) {
      logger.error(`Ocorreu um erro ao validar se era possível adicionar novo lançamento de venda ${JSON.stringify(venda)}`);
      callback(err, undefined);
    } else {
      logger.error(`A venda foi validade e será inserida no banco de dados ${JSON.stringify(venda)}`);
      let vendaDAO = self.getVendaDAO();
      vendaDAO.insereLancamentosVenda(venda, callback);
      self.close();
    }
  });
}

LancamentosVenda.prototype.criaLancamentoVenda = function(lancamentoVenda, callback) {
  //função implementada em common-lancamento-use
  this.insereLancamentoItemNoBanco(lancamentoVenda, validaInsercaoVenda.bind(this), callback);
}

LancamentosVenda.prototype.atualizaLancamentoVenda = function(id, lancamentoVenda, callback) {
  let self = this;
  //Deve fazer essa verificação, porque esse lançamento pode ter sido realizado em alguma plataforma
  //onde não é realizada a validação prévia do produto antes de enviar a alteração
  this.verificaExistenciaProdutoLancamento(lancamentoVenda.produto_id, function(err, retorno) {
    if (!err) {
      estoque.verificaEdicaoLancamentoVenda(lancamentoVenda, function(err, retorno) {
        if (!err) {
          let vendaDAO = self.getVendaDAO();
          //função implementada em common-crud-use
          self.atualizaDadoNoBanco(id, lancamentoVenda, vendaDAO.atualiza.bind(vendaDAO), callback);
        } else {
          callback(err, retorno);
        }
      });
    } else {
      callback(err, retorno);
    }
  });

}

LancamentosVenda.prototype.deletaLancamentoVenda = function(id, callback) {
  let vendaDAO = this.getVendaDAO();
  //função implementada em common-crud-use
  // O seguno parâmetro é undefind pq não precisamos fazer verificações para excluir um lançamento de venda
  this.deletaItemNoBanco(id, undefined, vendaDAO.deleta.bind(vendaDAO), callback);
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/lançamento de venda");

  common = app.api['common-database-use'];
  app.services['inherit-service'].makeInherit(LancamentosVenda, common);

  commonLancamento = app.api['common-lancamento-use'];
  app.services['inherit-service'].makeInherit(LancamentosVenda, commonLancamento);

  commonCrud = app.api['common-crud-use'];
  app.services['inherit-service'].makeInherit(LancamentosVenda, commonCrud);

  estoque = app.api.estoque;

  return new LancamentosVenda(app);

}

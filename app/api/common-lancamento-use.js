let logger;

function CommonLancamento(tipoMsg) {
  this._tipoMsg = tipoMsg;
}

CommonLancamento.prototype.insereLancamentoItemNoBanco = function(lancamentoItem, insertFunction, callback) {
  let self = this;
  this.verificaExistenciaProdutoLancamento(lancamentoItem.produto_id, function(err, retorno) {
    if (!err) {
      self.insereDadoNoBanco(lancamentoItem, insertFunction, callback);
    } else {
      callback(err, retorno);
    }
  });
}

CommonLancamento.prototype.verificaExistenciaProdutoLancamento = function(id, callback) {
  let self = this;
  logger.info(`Iniciando verificação se o produto existe, para ${self._tipoMsg}. ID = ${id}`);

  // A classe que implementar a CommonLancamento deverá disponobilar o método getProdutoDAO;
  let produtoDAO = this.getProdutoDAO();
  produtoDAO.listaProduto(id, function(err, retorno) {
    if (err) {
      logger.error(`Ocorreu um erro ao verificar se o produto estava presente na base de dados. ID = ${id}`);
      callback(err, retorno);
    } else if (retorno.length > 0) {
      logger.info(`Produto encontrado na base de dados. ID = ${id}`);
      callback(err, retorno);
    } else {
      let message = `Produto não foi encontrado na base de dados. Criação do ${self._tipoMsg} será cancelada. ID = ${id}`;
      logger.error(message);
      callback({data:[{message:message}], badRequest:true}, retorno);
    }
  });
  self.close();
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/common-lancamento-use");

  return CommonLancamento;
}

let logger;
let common;
let commonCrud;
function Produtos(app) {
  let infra = app.infra;
  common.call(this, infra, infra.ProdutosDAO, infra.EstoqueDAO);
  commonCrud.call(this, "Produto");
}

Produtos.prototype.getProdutoDAO = function() {
  //função implementada em common-database-use
  return this.newDAO();
}

Produtos.prototype.getEstoqueDAO = function() {
  //função implementada em common-database-use
  return this.newDAO(1);
}

Produtos.prototype.getProdutos = function(callback) {
  let produtosDAO = this.getProdutoDAO();
  //Função implementada em common-crud-use
  this.listarDadosDoBanco(produtosDAO.listaProdutos.bind(produtosDAO), callback);
}

Produtos.prototype.criaProduto = function(produto, callback) {
  let produtosDAO = this.getProdutoDAO();
  //Função implementada em common-crud-use
  this.insereDadoNoBanco(produto, produtosDAO.insereProduto.bind(produtosDAO), callback);
}

Produtos.prototype.atualizaProduto = function(id, produto, callback) {
  let produtosDAO = this.getProdutoDAO();
  //Função implementada em common-crud-use
  this.atualizaDadoNoBanco(id, produto, produtosDAO.atualiza.bind(produtosDAO), callback);
}

function verificaExclusaoProduto(id, callback) {
  logger.info('Iniciando a verificação da existência do produto nas tabelas de venda, estoque e desperdício');
  let estoqueDAO = this.getEstoqueDAO();
  estoqueDAO.quantidadeProdutosLancados(id, function(err, retorno) {
    // Faz uma consulta com count(*) na query, pra verificar quantas linhas aparecem
    // Com o produto a ser excluído na tabela de estoque
    let qtdLancamentosProduto = retorno[0]['count(*)'];
    if (qtdLancamentosProduto > 0) {
      logger.error(`Não foi possível excluir o produto ${id}. Ele já está presente na tabela de lançamentos em estoque`);
      callback([{message: `Não foi possível excluir o produto ${id} na base de dados. Ele possui lançamentos em estoque`}], retorno);
      return;
    }
    // Pela lógica, se o produto não for encontrado na tabela de lançamentos em estoque,
    // então ele não será encontrado nas tabelas de vendas e nem de desperdícios
    // porque apenas será possível lançar uma venda ou desperdício quando houver estoque
    // e somente será excluir lançamentos em estoque quando não deixar quantidade negativa
    // no estoque do produto
    // verificaExclusaoProdutoVendas.call(self, id, callback);
    callback(undefined, id);
  });
  this.close();
}

Produtos.prototype.deletaProduto = function(id, callback) {
  let produtosDAO = this.getProdutoDAO();
  //Função implementada em common-crud-use
  this.deletaItemNoBanco(id, verificaExclusaoProduto.bind(this), produtosDAO.deleta.bind(produtosDAO), callback);
}

module.exports = function(app) {
  logger = app.services.logger;
  logger.info("Exportando api/produtos");

  common = app.api['common-database-use'];
  app.services['inherit-service'].makeInherit(Produtos, common);

  commonCrud = app.api['common-crud-use'];
  app.services['inherit-service'].makeInherit(Produtos, commonCrud);

  return new Produtos(app);

}

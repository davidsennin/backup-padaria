function CommonDatabaseUse(infra, ...dao) {
  this._infra = infra;
  this._dao = dao;
  this._activeConnection = {};
}

CommonDatabaseUse.prototype.newDAO = function(index) {
  if (index !== undefined) {
    this.close(index);
    let d = this._dao[index];
    let c = this._activeConnection = this._infra.connectionFactory();
    return new d(c);
  }
  return this.newDAO(0);
}

CommonDatabaseUse.prototype.close = function(index) {
  if (this._activeConnection[index]) {
    this._activeConnection[index].end();
    this._activeConnection[index] = undefined;
  }
}

module.exports = function(app) {
  return CommonDatabaseUse;
}

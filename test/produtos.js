let express = require('../config/express')();
let request = require('supertest')(express);

describe('#ProdutosController', function() {

  it('#Listagem de produtos', function(done) {
    request.get('/produto')
      .expect('Content-type', /json/)
      .expect(200, done);

  });

  it('#Cadastro de novo produto com nome inválido', function (done) {
    request.post('/produto')
      .send({
        nome: "",
        custo: 4.99,
        preco: 5.99
      })
      .expect(400, done);
  });

  it('#Cadastro de novo produto com custo inválido', function (done) {
    request.post('/produto')
      .send({
        nome: "Nome do produto",
        custo: "A",
        preco: 5.99
      })
      .expect(400, done);
  });

  it('#Cadastro de novo produto com preço inválido', function (done) {
    request.post('/produto')
      .send({
        nome: "",
        custo: 4.99,
        preco: ""
      })
      .expect(400, done);
  });

  it('#Cadastro de novo produto com dados válidos', function (done) {
    request.post('/produto')
      .send({
        nome: "Nome do produto",
        custo: 4.99,
        preco: 5.99
      })
      .expect(302, done);
  });

  after(function() {
    let conn = express.infra.connectionFactory();
    conn.query("delete from produtos", function(err, result) {
      if (!err) {
        done();
      }
    });

  });

});

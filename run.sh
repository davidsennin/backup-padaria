docker rm -f $(docker ps -aqf "name=padaria-database")
docker network rm padarias 

docker network create padarias
( cd database ; sh ./run.sh; cd .. )

db_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' padaria-database)
sed -i -e "s/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/$db_ip/g" ./config/application.json

docker rm -f $(docker ps -aqf "name=padaria-server")
docker build -f Dockerfile -t davidjo/padaria .
docker run -d -p 8080:3003 --network=padarias --name "padaria-server" davidjo/padaria
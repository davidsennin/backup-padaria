CREATE SCHEMA `padarias_cadastros`;
use `padarias_cadastros`;
-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08-Fev-2019 às 05:01
-- Versão do servidor: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `padarias_cadastros`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `lancamento_desperdicio`
--

CREATE TABLE `lancamento_desperdicio` (
  `_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lancamento_estoque`
--

CREATE TABLE `lancamento_estoque` (
  `_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lancamento_estoque`
--

INSERT INTO `lancamento_estoque` (`_id`, `produto_id`, `quantidade`, `data`) VALUES
(41, 33, 3, '2018-10-27'),
(48, 41, 10, '2018-10-23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lancamento_venda`
--

CREATE TABLE `lancamento_venda` (
  `_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `custo` decimal(10,2) NOT NULL,
  `preco` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`_id`, `nome`, `custo`, `preco`) VALUES
(33, 'Leite', '2.99', '3.99'),
(41, 'Pão', '1.79', '4.99'),
(48, 'Biscoito', '2.99', '3.99'),
(52, 'Pudim', '1.99', '2.99');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lancamento_desperdicio`
--
ALTER TABLE `lancamento_desperdicio`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Indexes for table `lancamento_estoque`
--
ALTER TABLE `lancamento_estoque`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Indexes for table `lancamento_venda`
--
ALTER TABLE `lancamento_venda`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lancamento_desperdicio`
--
ALTER TABLE `lancamento_desperdicio`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lancamento_estoque`
--
ALTER TABLE `lancamento_estoque`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `lancamento_venda`
--
ALTER TABLE `lancamento_venda`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `lancamento_desperdicio`
--
ALTER TABLE `lancamento_desperdicio`
  ADD CONSTRAINT `lancamento_desperdicio_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`_id`);

--
-- Limitadores para a tabela `lancamento_estoque`
--
ALTER TABLE `lancamento_estoque`
  ADD CONSTRAINT `lancamento_estoque_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`_id`);

--
-- Limitadores para a tabela `lancamento_venda`
--
ALTER TABLE `lancamento_venda`
  ADD CONSTRAINT `lancamento_venda_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

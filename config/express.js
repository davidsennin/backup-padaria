let express = require('express');
let consign = require('consign');
let bodyParser = require('body-parser');
let expressValidator = require('express-validator');
let app = express();
let config = require('./application');

module.exports = function() {

  app.use('/node_modules', express.static('./node_modules'))
  app.use(express.static('./app/public'));
  app.use(bodyParser.json());
  app.use(expressValidator());
  app.config = config;

  consign()
    .include('services')
    .into(app);

  // app.services.logger.info(`Database info: `);
  // app.services.logger.info(process.env.DATABASE_SERVER.ip);
  // app.services.logger.info(process.env.DATABASE_SERVER.port);
  // app.services.logger.info(JSON.stringify(config));
  consign({cwd:'app'})
    .include('infra')
    .then('api/common-database-use')
    .then('api/common-lancamento-use')
    .then('api/common-crud-use')
    .then('api/estoque')
    .then('api')
    .then('controllers')
    .then('validadores')
    .then('routes')
    .into(app);

  return app;

}

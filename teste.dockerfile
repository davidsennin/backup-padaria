from node
maintainer David Oliveira
copy . /var/www
workdir /var/www

run npm install
entrypoint npm start
expose 3003
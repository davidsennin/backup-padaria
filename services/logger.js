let winston = require('winston');
let fs = require('fs');

if (!fs.existsSync('logs')) {
  fs.mkdirSync('logs');
}

module.exports = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/info.log",
      maxsize: 100000,
      maxFiles: 10
    }),
    new winston.transports.File({
      level: "error",
      filename: "logs/error.log",
      maxsize: 100000,
      maxFiles: 10
    })
  ]
});

let service = {};

service.makeInherit = function(son, dad) {
  //Meio padrão de fazer herança
  // var arrPro = Object.create(dad.prototype);
  // son.prototype = arrPro;
  // son.prototype.constructor = son;

  // Meio boca de fazer herança
  for (key in dad.prototype) {
    son.prototype[key] = dad.prototype[key];
  }
  son.prototype.constructor = son;
}

module.exports = function(app) {
  return service;
}

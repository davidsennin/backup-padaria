let memcached = require('memcached');

function createMemCachedClient() {
  let clienteMem = new memcached('localhost:11211', {
    retries: 0,//Número de retentativas feitas por request
    retry: 20,//tempo de espera em ms entre a falha de um servidor e a tentativa de colocá-lo em serviço
    remove: true,//autoriza o memcache a remover algum nó que esteja morto
    timeout: 100
  });

  return clienteMem;
}

// let c = createMemCachedClient();
// c.get('produtos', function(err, data) {
//   console.log(err);
//   console.log(data);
// });

module.exports = function() {
  return createMemCachedClient();
}

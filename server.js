let http = require('http');
let config = require('./config/express');
let app = config();

http.createServer(app)
  .listen(3003, function() {
    console.log('Servidor Padarias iniciado na porta 3003');
  });
